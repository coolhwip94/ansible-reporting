# Generating Report w/ Ansible
> This repo hosts a playbook to gather docker information from a remote host.

## Playbooks
- `playbooks/container_info.yml`
  - gathers docker host info of remote host
  - generates report file for individual container
  - concatenates report into single master report

- Running Playbook
  ```
  ansible-playbooks playbooks/container_info.yml
  ```

## Templates
- `templates/individual_report.j2`
  - template for creating report markdown per container
  - uses variables from playbook

## Reports
- example reports included in repo


